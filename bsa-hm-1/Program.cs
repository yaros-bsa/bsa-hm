﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using bsa_hm_1.Models;
using Newtonsoft.Json;

namespace bsa_hm_1
{
    class Program
    {
        private static readonly string _api = "https://bsa-dotnet.azurewebsites.net/api";

        private static readonly HttpClient client = new HttpClient();

        static void Main(string[] args)
        {
            // 37 - id user for test
            var task1 = GetTasksByUserId(37);
            var task2 = GetTasksLimitByNameAndByUserId(37);
            var task3 = GetTasksCurrentYearByUserId(37);
            var task4 = Task4();
            var task5 = GetUsersAndTasks();
            var task6 = GetUser(36);
            var task7 = GetProject();
        }

        // 1. Отримати кількість тасків у проекті конкретного користувача (по id) (словник, де key буде проект, а value кількість тасків).
        public static Dictionary<int, int> GetTasksByUserId(int userId)
        {
            var response = ApiCall($"{_api}/tasks");

            var tasks = JsonConvert.DeserializeObject<IList<Task>>(response);

            return tasks.Where(x => x.PerformerId == userId)
                .GroupBy(x => x.ProjectId)
                .Select(x => new { ProjectId = x.Key, Count = x.Count() })
                .ToDictionary(x => x.ProjectId, x => x.Count);
        }

        // 2. Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів (колекція з тасків).
        public static IList<Task> GetTasksLimitByNameAndByUserId(int userId)
        {
            var response = ApiCall($"{_api}/tasks");

            var tasks = JsonConvert.DeserializeObject<IList<Task>>(response);

            return tasks.Where(x => x.PerformerId == userId && x.Name.Length < 10).ToList();
        }

        // 3. Отримати список (id, name) з колекції тасків, які виконані (finished) в поточному (2021) році для конкретного користувача (по id).
        public static IList<Task> GetTasksCurrentYearByUserId(int userId, int currentYear = 2021)
        {
            var response = ApiCall($"{_api}/tasks");

            var tasks = JsonConvert.DeserializeObject<IList<Task>>(response);

            return tasks.Where(x => x.PerformerId == userId && Convert.ToDateTime(x.FinishedAt).Year == currentYear)
                .Select(task => new Task { Id = task.Id, Name = task.Name }).ToList();
        }

        // 4. Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років, відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.
        public static IList<TeamTask4> Task4()
        {
            var teams = JsonConvert.DeserializeObject<IList<Team>>(ApiCall($"{_api}/teams"));
            var users = JsonConvert.DeserializeObject<IList<User>>(ApiCall($"{_api}/users"));

            return teams
                .Join(users,
                        team => team.Id,
                        user => user.TeamId,
                        (team, user) => new { Id = team.Id, Name = team.Name, User = user})
                .Where(x => DateTime.Now.AddYears(-10) > x.User.BirthDay)
                .OrderByDescending(user => user.User.RegisteredAt)
                .GroupBy(team => team.Id)
                .Select(group => new TeamTask4 { Id = group.Key, Name = group.FirstOrDefault().Name, User = group.Select(user => user.User).ToList()})
                .ToList();
        }

        // 5. Отримати список користувачів за алфавітом first_name (по зростанню) з відсортованими tasks по довжині name (за спаданням).
        public static IList<Task5Model> GetUsersAndTasks()
        {
            var tasks = JsonConvert.DeserializeObject<IList<Task>>(ApiCall($"{_api}/tasks"));
            var users = JsonConvert.DeserializeObject<IList<User>>(ApiCall($"{_api}/users"));

            var result = tasks
                .Join(users,
                        tasks => tasks.PerformerId,
                        user => user.Id,
                        (tasks, user) => new { User = user, Tasks = tasks })
                .GroupBy(x => x.User)
                .Select(group => new Task5Model { User = group.Key, Tasks = group.Select(x => x.Tasks).OrderByDescending(x => x.Name.Length).ToList() })
                .OrderBy(x => x.User.FirstName)
                .ToList();

            return result;
        }

        // 6. Отримати наступну структуру (передати Id користувача в параметри):
        //      User
        //      Останній проект користувача(за датою створення)
        //      Загальна кількість тасків під останнім проектом
        //      Загальна кількість незавершених або скасованих тасків для користувача
        //      Найтриваліший таск користувача за датою(найраніше створений - найпізніше закінчений)

        public static string GetUser(int userId)
        {
            var tasks = JsonConvert.DeserializeObject<IList<Task>>(ApiCall($"{_api}/tasks"));
            var users = JsonConvert.DeserializeObject<IList<User>>(ApiCall($"{_api}/users"));
            var projects = JsonConvert.DeserializeObject<IList<Project>>(ApiCall($"{_api}/projects"));

            var result = users
                .Where(x => x.Id == userId)
                .Join(projects,
                        user => user.Id,
                        project => project.AuthorId,
                        (user, project) => new { User = user, Project = project })
                .GroupBy(x => x.User)
                .Select(group => new {
                    User = group.Key,
                    LastProject = group.Select(x => x.Project).OrderByDescending(x => x.CreatedAt).FirstOrDefault()
                })
                .Join(tasks,
                        x => x.User.Id,
                        tasks => tasks.PerformerId,
                        (x, tasks) => new { x.User, x.LastProject, Tasks = tasks })
                .GroupBy(x => x.User)
                .Select(group => new {
                    User = group.Key,
                    LastProject = group.Select(x => x.LastProject).FirstOrDefault(),
                    TotalTasksForProject = group.Select(x => x.Tasks).Where(x => x.ProjectId == group.Select(x => x.LastProject).FirstOrDefault().Id).Count(),
                    TotalStartedOrCanceledTasks = group.Select(x => x.Tasks).Where(x => x.State == TaskState.Start || x.State == TaskState.Cancel).Count(),
                    LongestTask = group.Select(x => x.Tasks).Where(x => x.FinishedAt != null).Aggregate((x, y) =>
                        {
                            return x.FinishedAt - x.CreatedAt > y.FinishedAt - y.CreatedAt ? x : y;
                        })
                })
                .ToList();

            return JsonConvert.SerializeObject(result);
        }

        // 7. Отримати таку структуру:
        //      Проект
        //      Найдовший таск проекту (за описом)
        //      Найкоротший таск проекту (по імені)
        //      Загальна кількість користувачів в команді проекту, де або опис проекту >20 символів, або кількість тасків <3
        public static string GetProject()
        {
            var tasks = JsonConvert.DeserializeObject<IList<Task>>(ApiCall($"{_api}/tasks"));
            var users = JsonConvert.DeserializeObject<IList<User>>(ApiCall($"{_api}/users"));
            var projects = JsonConvert.DeserializeObject<IList<Project>>(ApiCall($"{_api}/projects"));
            var teams = JsonConvert.DeserializeObject<IList<Team>>(ApiCall($"{_api}/teams"));

            var result = projects
                    .Join(tasks,
                            project => project.Id,
                            task => task.ProjectId,
                            (project,  task) => new { Project = project, Task = task })
                    .GroupBy(x => x.Project)
                    .Select(group => new {
                        Project = group.Key,
                        Task = group.Select(x => x.Task),
                        LongestDescriptionTaskProject = group.Select(x => x.Task).Where(x => x.ProjectId == group.Key.Id).OrderByDescending(x => x.Description.Length).FirstOrDefault(),
                        ShortestNameTaskProject = group.Select(x => x.Task).Where(x => x.ProjectId == group.Key.Id).OrderBy(x => x.Name.Length).FirstOrDefault(),
                    })
                    .Join(users,
                            project => project.Project.TeamId,
                            user => user.TeamId,
                            (project, user) => new { Project = project, User = user })
                    .GroupBy(x => x.Project)
                    .Select(group => new {
                        group.Key.Project,
                        group.Key.LongestDescriptionTaskProject,
                        group.Key.ShortestNameTaskProject,
                        TotalUsers = group.Select(x => x.User)
                            .Where(x => x.Id == group.Key.Project.AuthorId && (group.Key.Project.Description.Length > 20 || group.Key.Task.ToList().Count() < 3)).ToList().Count()
                    })
                .ToList();

            return JsonConvert.SerializeObject(result);
        }


        private static string ApiCall(string requestUrl)
        {
            var responseTask = client.GetAsync(requestUrl).Result;
            return responseTask.Content.ReadAsStringAsync().Result;
        }
    }
}
