﻿using System;
using System.Collections.Generic;

namespace bsa_hm_1.Models
{
    public class Task5Model
    {
        public User User { get; set; }
        public IList<Task> Tasks { get; set; }
    }
}
