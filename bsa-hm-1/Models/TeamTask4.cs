﻿using System;
using System.Collections.Generic;

namespace bsa_hm_1.Models
{
    public class TeamTask4
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public IList<User> User { get; set; }
    }
}
