﻿using System;
namespace bsa_hm_1.Models
{
    public class Task
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public string Name { get; set; }

        public string Description { get; set; }

        public TaskState State { get; set; }

        public DateTime? CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }
    }

    public enum TaskState
    {
        Start,
        End,
        Current,
        Cancel
    }
}
